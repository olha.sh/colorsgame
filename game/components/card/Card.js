import React from 'react';
import {Image, TouchableOpacity, StyleSheet} from 'react-native'

function Card ({idKey, src, chooseCard}) {
    
function Choose (){
    chooseCard(idKey)
}

    return(
        <TouchableOpacity style={styles.cardBorder} key={idKey} onPress={Choose}>
            <Image source={src} 
                style={styles.image}
            /> 
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    cardBorder: {
        width: 140,
        height: 140,
        borderRadius: 40,
        borderWidth: 4,
        borderColor: "#fff",
        margin: 10,
    },
    image:{
        width: 120,
        height: 120,
        borderRadius: 40,

    }
})

export{
    Card
}