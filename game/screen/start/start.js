import React from 'react';
import {StyleSheet, Text, TouchableOpacity, TouchableHighlight} from 'react-native'
import {BoxShadow} from 'react-native-shadow'



function StartScreen({handleStart}){
 function runGame(){
     handleStart()
 }

    return(
      <TouchableOpacity 
                style={styles.playBtn}
                onPress={runGame}
            > 
                <Text style={styles.playText}>
                    Play
                </Text> 
        </TouchableOpacity>
        
    )
}

const styles = StyleSheet.create({
    playBtn:{
        width: 250,
        height: 150,
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 4,
        borderColor: "#fff",
},
    playText:{
        color: '#ffff',
        fontSize: 28,
        fontWeight: "bold",
        textTransform: 'uppercase',
    }
})

export{
    StartScreen
}